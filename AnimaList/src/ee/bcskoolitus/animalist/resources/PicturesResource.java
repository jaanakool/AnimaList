package ee.bcskoolitus.animalist.resources;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ee.bcskoolitus.animalist.dao.Picture;

public class PicturesResource {
	public static List<Picture> getAllPictures() {
		List<Picture> allPictures = new ArrayList<>();
		String sqlQuery = "SELECT * FROM pictures;";
		try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);) {
			while (results.next()) {
				Picture picture = new Picture();
				picture.setPictureId(results.getInt("picture_id"));
				picture.setPictureArtist(ArtistsResource.getArtistById(results.getInt("picture_artist_id")));
				picture.setPictureName(results.getString("picture_name"));
				picture.setPictureMeasures(results.getString("picture_measures"));
				picture.setPictureWeight(results.getInt("picture_weight"));
				picture.setPicturePrice(results.getInt("picture_price"));
				picture.setPictureStatus(results.getString("picture_status"));
				picture.setPictureJpg(results.getString("picture_jpg"));
				allPictures.add(picture);
			}
		} catch (SQLException e) {
			System.out.println("Error getting all pictures: " + e.getMessage());
		}
		return allPictures;
	}

	public static List<Picture> getPictureById(int pictureArtist) {
		List<Picture> allArtistPictures = new ArrayList<>();
		String sqlQuery = "SELECT * FROM pictures WHERE picture_artist_id=" + pictureArtist;
		try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);) {
			while (results.next()) {
				Picture picture = new Picture();
				picture.setPictureArtist(ArtistsResource.getArtistById(results.getInt("picture_artist_id")));
				picture.setPictureId(results.getInt("picture_id"));
				picture.setPictureName(results.getString("picture_name"));
				picture.setPictureMeasures(results.getString("picture_measures"));
				picture.setPictureWeight(results.getInt("picture_weight"));
				picture.setPicturePrice(results.getInt("picture_price"));
				picture.setPictureStatus(results.getString("picture_status"));
				picture.setPictureJpg(results.getString("picture_jpg"));
				allArtistPictures.add(picture);
			}
		} catch (SQLException e) {
			System.out.println("Error getting one picture: " + e.getMessage());
		}
		return allArtistPictures;
	}

	public static List<Picture> getAllImages() {
		List<Picture> allImages = new ArrayList<>();
		String sqlQuery = "SELECT picture_jpg FROM pictures;";
		try (Connection connection = DatabaseConnection.getConnection();
				ResultSet results = connection.createStatement().executeQuery(sqlQuery);) {
			while (results.next()) {
				Picture picture = new Picture();
				picture.setPictureJpg(results.getString("picture_jpg"));
				allImages.add(picture);
			}			
		} catch (SQLException e) {
			System.out.println("Error getting all images: " + e.getMessage());
		}
		return allImages;

	}
}