package ee.bcskoolitus.animalist.resources;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ee.bcskoolitus.animalist.dao.Artist;

public class ArtistsResource {

	public static List<Artist> getAllArtists() {
		List<Artist> allArtists = new ArrayList<>();
		String sqlQuery = "SELECT * FROM artists;";
		try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);) {
			while (results.next()) {
				Artist artist = new Artist();
				artist.setArtistId(results.getInt("artist_id"));
				artist.setArtistName(results.getString("artist_name"));
				artist.setArtistMail(results.getString("artist_mail"));
				artist.setArtistPhone(results.getString("artist_phone"));
				artist.setArtistHtml(results.getString("artist_html"));
				allArtists.add(artist);
			}
		} catch (SQLException e) {
			System.out.println("Error getting all artists: " + e.getMessage());
		}
		return allArtists;
	}

	public static Artist addArtist(Artist newArtist) {
		String sqlQuery = "INSERT INTO artists " + "(artist_name, artist_mail, artist_phone, artist_html) "
				+ "VALUES ('" + newArtist.getArtistName() + "', '" + newArtist.getArtistMail() + "', '"
				+ newArtist.getArtistPhone() + "', '" + newArtist.getArtistHtml() + "')";
		try (Statement statement = DatabaseConnection.getConnection().createStatement();) {
			Integer resultCode = statement.executeUpdate(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			if (resultCode == 1) {
				ResultSet resultSet = statement.getGeneratedKeys();
				while (resultSet.next()) {
					newArtist.setArtistId(resultSet.getInt(1));
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return newArtist;
	}

	public static boolean deleteArtistById(int artistId) {
		boolean isDeleteSuccessful = false;
		String sqlQuery = "DELETE FROM artists " + "WHERE artist_id=" + artistId;

		try (Statement statement = DatabaseConnection.getConnection().createStatement();) {
			Integer resultCode = statement.executeUpdate(sqlQuery);
			if (resultCode == 1) {
				isDeleteSuccessful = true;
			}
		} catch (SQLException e) {
			System.out.println("Error on deleting artist");
		}
		return isDeleteSuccessful;

	}

	public static boolean updateArtist(Artist artistToUpdate) {
		boolean isUpdateSuccessful = false;
		String sqlQuery = "UPDATE artists SET artist_name='" + artistToUpdate.getArtistName() + "', artist_mail='"
				+ artistToUpdate.getArtistMail() + "', artist_phone='" + artistToUpdate.getArtistPhone()
				+ "', artist_html='" + artistToUpdate.getArtistHtml() + "' WHERE artist_id="
				+ artistToUpdate.getArtistId();

		System.out.println(sqlQuery);
		try (Statement statement = DatabaseConnection.getConnection().createStatement();) {
			Integer resultCode = statement.executeUpdate(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			if (resultCode == 1) {
				isUpdateSuccessful = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return isUpdateSuccessful;
	}

	public static Artist getArtistById(int artistId) {
		Artist artist = new Artist();
		String sqlQuery = "SELECT * FROM artists WHERE artist_id=" + artistId;
		try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);) {
			while (results.next()) {
				artist.setArtistId(results.getInt("artist_id"));
				artist.setArtistName(results.getString("artist_name"));
			}
		} catch (SQLException e) {
			System.out.println("Error getting one artist: " + e.getMessage());
		}
		return artist;
	}
}
