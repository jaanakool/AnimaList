package ee.bcskoolitus.animalist.resources;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ee.bcskoolitus.animalist.dao.Client;

public class ClientsResource {

	public static List<Client> getAllClients() {
		List<Client> allClients = new ArrayList<>();
		String sqlQuery = "SELECT * FROM clients;";
		try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);) {
			while (results.next()) {
				Client client = new Client();
				client.setPictureId(results.getInt("picture_id"));
				client.setClientName(results.getString("client_name"));
				client.setClientMail(results.getString("client_mail"));
				client.setClientAddress(results.getString("client_address"));
				client.setClientPhone(results.getString("client_phone"));
				client.setDeliveryCost(results.getInt("delivery_cost"));
				client.setTotalCost(results.getInt("total_cost"));
				allClients.add(client);
			}
		} catch (SQLException e) {
			System.out.println("Error getting all clients: " + e.getMessage());
		}
		return allClients;
	}

	public static Client addClient(Client newClient) {
		int totalCost = getTotalCost(newClient.getPictureId(), newClient.getDeliveryCost());
		if (totalCost > 0) {
			String sqlQuery = "INSERT INTO clients "
					+ "(picture_id, client_name, client_mail, client_address, client_phone, delivery_cost, total_cost) "
					+ "VALUES (" + newClient.getPictureId() + ", '" + newClient.getClientName() + "', '"
					+ newClient.getClientMail() + "', '" + newClient.getClientAddress() + "', '"
					+ newClient.getClientPhone() + "', " + newClient.getDeliveryCost() + ", " + totalCost + ")";
			try (Statement statement = DatabaseConnection.getConnection().createStatement();) {
				Integer resultCode = statement.executeUpdate(sqlQuery);
				if (resultCode == 1) {
					newClient.setTotalCost(totalCost);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return newClient;
		}
		return null;
	}

	private static int getTotalCost(int pictureId, int deliveryCost) {
		String sqlQuery = "SELECT picture_price FROM pictures WHERE picture_id = " + pictureId;
		int totalCost = 0;
		try (Statement statement = DatabaseConnection.getConnection().createStatement();) {
			ResultSet results = statement.executeQuery(sqlQuery);
			results.next();
			totalCost = results.getInt("picture_price") + deliveryCost;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return totalCost;
	}

}
