package ee.bcskoolitus.animalist.controller;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ee.bcskoolitus.animalist.dao.Picture;
import ee.bcskoolitus.animalist.resources.PicturesResource;

@Path("/pictures")
public class PictureController {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Picture> getAllPictures() {
		return PicturesResource.getAllPictures();
	}

	@GET
	@Path("/{pictureArtist}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Picture> getPictureById(@PathParam("pictureArtist") int pictureArtistId) {
		return PicturesResource.getPictureById(pictureArtistId);
	}

	@GET
	@Path("/images")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Picture> getAllImages() {
		return PicturesResource.getAllImages();
	}
}
