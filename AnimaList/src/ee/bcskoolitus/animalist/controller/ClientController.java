package ee.bcskoolitus.animalist.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ee.bcskoolitus.animalist.dao.Client;
import ee.bcskoolitus.animalist.resources.ClientsResource;

@Path("/clients")

public class ClientController {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Client> getAllClients() {
		return ClientsResource.getAllClients();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Client addNewClient(Client newClient) {
		return ClientsResource.addClient(newClient);
	}

}
