package ee.bcskoolitus.animalist.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ee.bcskoolitus.animalist.dao.Artist;
import ee.bcskoolitus.animalist.resources.ArtistsResource;

@Path("/artists")
public class ArtistController {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Artist> getAllArtists() {
		return ArtistsResource.getAllArtists();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Artist addNewArtist(Artist newArtist) {
		return ArtistsResource.addArtist(newArtist);
	}

	@DELETE
	@Path("/{artistId}")
	public void deleteArtistById(@PathParam("artistId") int artistId) {
		ArtistsResource.deleteArtistById(artistId);
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN) public boolean updateArtist(Artist artist) { 
		return ArtistsResource.updateArtist(artist);
	} 
	

}