package ee.bcskoolitus.animalist.dao;

public class Picture {

	private int pictureId;

	private Artist pictureArtist;

	private String pictureName;

	private String pictureMeasures;

	private int pictureWeight;

	private int picturePrice;

	private String pictureStatus;

	private String pictureJpg;

	public int getPictureId() {
		return pictureId;
	}

	public void setPictureId(int pictureId) {
		this.pictureId = pictureId;
	}

	public Artist getPictureArtist() {
		return pictureArtist;
	}

	public void setPictureArtist(Artist pictureArtist) {
		this.pictureArtist = pictureArtist;
	}

	public String getPictureName() {
		return pictureName;
	}

	public void setPictureName(String pictureName) {
		this.pictureName = pictureName;
	}

	public String getPictureMeasures() {
		return pictureMeasures;
	}

	public void setPictureMeasures(String pictureMeasures) {
		this.pictureMeasures = pictureMeasures;
	}

	public int getPictureWeight() {
		return pictureWeight;
	}

	public void setPictureWeight(int pictureWeight) {
		this.pictureWeight = pictureWeight;
	}

	public int getPicturePrice() {
		return picturePrice;
	}

	public void setPicturePrice(int picturePrice) {
		this.picturePrice = picturePrice;
	}

	public String getPictureStatus() {
		return pictureStatus;
	}

	public void setPictureStatus(String pictureStatus) {
		this.pictureStatus = pictureStatus;
	}

	public String getPictureJpg() {
		return pictureJpg;
	}

	public void setPictureJpg(String pictureJpg) {
		this.pictureJpg = pictureJpg;
	}

	@Override
	public String toString() {
		return "Pictures[pictureId=" + pictureId + ", pictureArtist=" + pictureArtist + ", pictureName=" + pictureName
				+ ", pictureMeasures=" + pictureMeasures + ", pictureWeight=" + pictureWeight + ", picturePrice="
				+ picturePrice + "pictureStatus=" + pictureStatus + ", pictureJpg=" + pictureJpg + "]";
	}

}
