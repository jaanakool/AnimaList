package ee.bcskoolitus.animalist.dao;

public class Client {
	private int pictureId;
	private String clientName;
	private String clientMail;
	private String clientAddress;
	private String clientPhone;
	private int deliveryCost;
	private int totalCost;

	public int getPictureId() {
		return pictureId;
	}

	public void setPictureId(int pictureId) {
		this.pictureId = pictureId;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getClientMail() {
		return clientMail;
	}

	public void setClientMail(String clientMail) {
		this.clientMail = clientMail;
	}

	public String getClientAddress() {
		return clientAddress;
	}

	public void setClientAddress(String clientAddress) {
		this.clientAddress = clientAddress;
	}

	public String getClientPhone() {
		return clientPhone;
	}

	public void setClientPhone(String clientPhone) {
		this.clientPhone = clientPhone;
	}

	public int getDeliveryCost() {
		return deliveryCost;
	}

	public void setDeliveryCost(int deliveryCost) {
		this.deliveryCost = deliveryCost;
	}

	public int getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(int totalCost) {
		this.totalCost = totalCost;
	}

	@Override
	public String toString() {
		return "Client[pictureId=" + pictureId + ", clientName=" + clientName + ", clientMail=" + clientMail
				+ ", clientAddress=" + clientAddress + ", clientPhone=" + clientPhone + ", deliveryCost=" + deliveryCost
				+ ", totalCost=" + totalCost + "]";
	}

}
