package ee.bcskoolitus.animalist.dao;

import java.math.BigDecimal;

public class DeliveryCost {
	private int deliveryPriceId;
	private BigDecimal deliveryPrice;
	private String deliveryMethod;
	private BigDecimal deliveryCharge;
	private BigDecimal deliveryCost;
	private int deliveryId;

	public int getDeliveryPriceId() {
		return deliveryPriceId;
	}

	public void setDeliveryPriceId(int deliveryPriceId) {
		this.deliveryPriceId = deliveryPriceId;
	}

	public BigDecimal getDeliveryPrice() {
		return deliveryPrice;
	}

	public void setDeliveryPrice(BigDecimal deliveryPrice) {
		this.deliveryPrice = deliveryPrice;
	}

	public String getDeliveryMethod() {
		return deliveryMethod;
	}

	public void setDeliveryMethod(String deliveryMethod) {
		this.deliveryMethod = deliveryMethod;
	}

	public BigDecimal getDeliveryCharge() {
		return deliveryCharge;
	}

	public void setDeliveryCharge(BigDecimal deliveryCharge) {
		this.deliveryCharge = deliveryCharge;
	}

	public BigDecimal getDeliveryCost() {
		return deliveryCost;
	}

	public void setDeliveryCost(BigDecimal deliveryCost) {
		this.deliveryCost = deliveryCost;
	}

	public int getDeliveryId() {
		return deliveryId;
	}

	public void setDeliveryId(int deliveryId) {
		this.deliveryId = deliveryId;
	}

	@Override
	public String toString() {
		return "DeliveryCost[deliveryPriceId=" + deliveryPriceId + ", deliveryPrice=" + deliveryPrice
				+ ", deliveryMethod=" + deliveryMethod + 
				 ", deliveryCharge=" + deliveryCharge + 
				 ", deliveryCost=" + deliveryCost + 
				 ", deliveryId=" + deliveryId + "]";
		
	}
}
