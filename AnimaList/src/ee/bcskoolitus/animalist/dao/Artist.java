package ee.bcskoolitus.animalist.dao;

public class Artist {
	private int artistId;
	private String artistName;
	private String artistMail;
	private String artistPhone;
	private String artistHtml;

	public int getArtistId() {
		return artistId;
	}

	public void setArtistId(int artistId) {
		this.artistId = artistId;
	}

	public String getArtistName() {
		return artistName;
	}

	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}

	public String getArtistMail() {
		return artistMail;
	}

	public void setArtistMail(String artistMail) {
		this.artistMail = artistMail;
	}

	public String getArtistPhone() {
		return artistPhone;
	}

	public void setArtistPhone(String artistPhone) {
		this.artistPhone = artistPhone;
	}

	public String getArtistHtml() {
		return artistHtml;
	}

	public void setArtistHtml(String artistHtml) {
		this.artistHtml = artistHtml;
	}

	@Override
	public String toString() {
		return "Artist[artistId=" + artistId + ", artistName=" + artistName + ", artistMail=" + artistMail
				+ ", artistPhone=" + artistPhone + "]";
	}

}
