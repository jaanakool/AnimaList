var allArtistsList;
function getAllArtistsAndFillList() {
	$
			.getJSON(
					"http://localhost:8080/AnimaList/rest/artists",
					function(allArtists) {
						allArtistsList = allArtists;
						var listItems = "";
						for (var i = 0; i < allArtists.length; i++) {
							listItems = listItems
									+ "<li><h3><a href='"
									+ allArtists[i].artistHtml
									+ "'>"
									+ allArtists[i].artistName
									+ "</a></h3><p>phone "
									+ allArtists[i].artistPhone
									+ "</p><p>"
									+ allArtists[i].artistMail
									+ "</p><p></p></li>"
									+ "</td><td><button type='button' onClick='deleteArtist("
									+ allArtists[i].artistId
									+ ")'>Delete</button><button type='button' onClick='modifyArtist("
									+ allArtists[i].artistId
									+ ")'>Modify</button></td><tr>";
						}
						document.getElementById("services").innerHTML = listItems;
					});
}

getAllArtistsAndFillList();

function deleteArtist(artistId) {

	$.ajax({
		url : "http://localhost:8080/AnimaList/rest/artists/" + artistId,
		type : "DELETE",
		contentType : "application/json;charset=utf-8",
		success : function() {
			getAllArtistsAndFillList();
		},
		error : function(XMLHTTPRequest, textStatus, errorThrown) {
			console.log("error on deleting artist");
		}
	});
}

function modifyArtist(artistId) {
	for (var i = 0; i < allArtistsList.length; i++) {
		if (allArtistsList[i].artistId == artistId) {
			document.getElementById("modifyArtistId").value = artistId;
			document.getElementById("modifyArtistName").value = allArtistsList[i].artistName;
			document.getElementById("modifyArtistMail").value = allArtistsList[i].artistMail;
			document.getElementById("modifyArtistPhone").value = allArtistsList[i].artistPhone;
			document.getElementById("modifyArtistHtml").value = allArtistsList[i].artistHtml;
		}
	}
}

function modisaveArtist(artistId) {
	if (document.getElementById("modifyArtistId").value != undefined
			&& document.getElementById("modifyArtistId").value != "") {
		return saveArtistChanges();
	} else {
		return saveNewArtist();
	}

}

function saveNewArtist() {
	var jsonData = {
		"artistName" : document.getElementById("modifyArtistName").value,
		"artistMail" : document.getElementById("modifyArtistMail").value,
		"artistPhone" : document.getElementById("modifyArtistPhone").value,
		"artistHtml" : document.getElementById("modifyArtistHtml").value
	};
	var jsonDataString = JSON.stringify(jsonData);
	$.ajax({
		url : "http://localhost:8080/AnimaList/rest/artists/",
		type : "POST",
		data : jsonDataString,
		contentType : "application/json;charset=utf-8",
		success : function() {
			getAllArtistsAndFillList;
		},
		error : function(XMLHTTPRequest, textStatus, errorThrown) {
			console.log("error on adding new artist");
		}
	});
}
function saveArtistChanges() {
	var jsonData = {
		"artistId" : document.getElementById("modifyArtistId").value,
		"artistName" : document.getElementById("modifyArtistName").value,
		"artistMail" : document.getElementById("modifyArtistMail").value,
		"artistPhone" : document.getElementById("modifyArtistPhone").value,
		"artistHtml" : document.getElementById("modifyArtistHtml").value
	};
	var jsonDataString = JSON.stringify(jsonData);
	$.ajax({
		url : "http://localhost:8080/AnimaList/rest/artists/",
		type : "PUT",
		data : jsonDataString,
		contentType : "application/json;charset=utf-8",
		success : function() {
			getAllArtistsAndFillList();
		},
		error : function(XMLHTTPRequest, textStatus, errorThrown) {
			console.log("error on adding new artist");
		}
	});
}