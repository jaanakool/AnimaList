var allPicturesList;
function getAllPicturesAndFillList() {
	$
			.getJSON(
					"http://localhost:8080/AnimaList/rest/pictures",
					function(allPictures) {
						allPicturesList = allPictures;
						var listItems = "";
						for (var i = 0; i < allPictures.length; i++) {
							listItems = listItems
									+ "<li><p>"
									+ "<img src='images/"
									+ allPictures[i].pictureJpg
									+ "'></p><p><strong>"
									+ allPictures[i].pictureName
									+ "</strong> : <strong>"
									+ allPictures[i].pictureArtist.artistName
									+ "</strong><p><h6></p>measures "
									+ allPictures[i].pictureMeasures
									+ " mm"
									+ ", weight "
									+ allPictures[i].pictureWeight
									+ " g"
									+ ", price "
									+ allPictures[i].picturePrice
									+ " €"
									+ "</p><p></p>"
									+ allPictures[i].pictureStatus
									+ "</p><h6>"
									+ "<button type='button' class='btn btn-primary' data-toggle='modal' data-target='#buyItem' onClick='buyPicture("
									+ allPictures[i].pictureId
									+ ")'>Buy</button></li>";
						}
						document.getElementById("services").innerHTML = listItems;
					});
}

getAllPicturesAndFillList();

function buyPicture(pictureId) {
	for (var i = 0; i < allPicturesList.length; i++) {
		if (allPicturesList[i].pictureId == pictureId) {
			document.getElementById("pictureName").innerHTML = allPicturesList[i].pictureName;
			document.getElementById("artistName").innerHTML = allPicturesList[i].pictureArtist.artistName;
			document.getElementById("picturePrice").innerHTML = allPicturesList[i].picturePrice;
			document.getElementById("buyButtonInModal").setAttribute("onClick",
					"finalizePictureBuying(" + pictureId + ")");

		}

	}
}

$("input[name='delivery']").change(function(){
	var sum = parseInt(document.getElementById("picturePrice").innerHTML) + parseInt(document.querySelector("input[name='delivery']:checked").value);
	document.getElementById("totalPrice").value = sum;
});

function finalizePictureBuying(pictureId) {
	var clientDataJson = {
		"pictureId" : pictureId,
		"clientName" : document.getElementById("insertContactName").value,
		"clientMail" : document.getElementById("insertContactMail").value,
		"clientAddress" : document.getElementById("insertContactAddress").value,
		"clientPhone" : document.getElementById("insertContactPhone").value,
		"deliveryCost" : document.querySelector("input[name='delivery']:checked").value
		

		};
		var clientDataJsonString = JSON.stringify(clientDataJson);
		$.ajax({
			url : "http://localhost:8080/AnimaList/rest/clients/",
			type : "POST",
			data : clientDataJsonString,
			contentType : "application/json;charset=utf-8",
			success : function() {
				alert ("You have purchased a masterpiece!");
			},
			error : function(XMLHTTPRequest, textStatus, errorThrown) {
				console.log("error on adding new client");
			}
		});
		
		
		// get list of radio buttons with name 'size'
		var deliveries = document.forms["deliveries"].elements["delivery"];

		// loop through list
		for (var i=0, len=deliveries.length; i<len; i++) {
		    deliveries[i].onclick = function() { // assign onclick handler function to each
		        // put clicked radio button's value in total field
		        this.form.elements.total.value = this.value;
		    };
		}
	
}
